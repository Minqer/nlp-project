import re
from collections import Counter
from nltk.metrics import *

def words(text): return re.findall(r'\w+', text.lower())

WORDS = Counter(words(open('dictionary.txt').read()))

def Prob(word, N=sum(WORDS.values())):
    "Probability of `word`."
    return WORDS[word] / N

def selectWord(word):
    "Generate possible spelling corrections for word."
    "Select the word that has the most less edit distance between each word in dicitonary and word."
    maxEdit = 3
    corWord = word
    for ws in WORDS:
        ed = edit_distance(word, ws)
        if maxEdit > ed:
            maxEdit = ed
            corWord = ws
    return [corWord]

def correctWord(word):
    "Most probable spelling correction for word."
    return max(selectWord(word), key=Prob)
