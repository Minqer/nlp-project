from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from spellCheck import correctWord

app = Flask(__name__)
api = Api(app)

class checkSpell(Resource):
    def get(self, sentence):
        sentence = sentence.split(' ')
        correctedSentence = ''
        for s in sentence:
            correctedSentence += correctWord(s) + ' '

        correctedSentence = correctedSentence.rstrip()
        return {'sentence': correctedSentence}

api.add_resource(checkSpell, '/checkSpell/<sentence>') # Route_1


if __name__ == '__main__':
     app.run(port='5002')
